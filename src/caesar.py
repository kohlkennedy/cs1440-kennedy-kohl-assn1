import fileinput
import string
import sys
'This is the function to shift the text'
def shift(text, amount):
    
    decoded = ""
    print("=======================")
    print("Rotated by " + str(amount) + " positions")
    print("=======================")
    'For loop that goes through each character'
    for char in text:
        letr = 0
        'If lowercase, goes through this loop'
        if char.islower():
            'Converts the letter to a number'
            num = ord(char)
            'Shifts the letter by this amount'
            letr = num + amount
            if letr > 122:
                letr = (letr - 122) + 96
        elif char.isupper():
            '''Converts the letter to a number'''
            num = ord(char)
            letr = num + amount
            if letr > 90:
                letr = (letr - 90) + 64
        else:
            letr = ord(char)
        decoded += chr(letr)
    print(decoded)

'Makes the arguments in the command line an array'
arguments = sys.argv
'Makes file equal to the file text'
file = open(arguments[1])
'Makes text equal to the file'
text = file.read()
'If there\'s no second argument, goes through all 26 shifts'
if(len(arguments) == 2):
    for i in range(0,26):
        shift(text, int(i))
else:
    'Sets x to the shift amount'
    x = arguments[2]
    if(int(x) < 26 and int(x) >= 0):
        shift(text, int(x))
        """If x isn\'t an acceptable letter, it prints out an error statement"""
    else: 
        print("Sorry Bud, but that's an out of bounds character")